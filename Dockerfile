FROM golang:alpine as builder
ENV GO111MODULE=on
RUN apk update && \
    apk upgrade && \
    apk add git gcc libc-dev linux-headers
RUN go get -ldflags "-X main.VERSION=$(date -u +%Y%m%d) -s -w" github.com/xtaci/kcptun/server

FROM alpine:3.9
COPY --from=builder /go/bin /bin

CMD ["server", "-c", "/app/config.json"]